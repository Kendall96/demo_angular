const Sequelize = require('sequelize');
const sequelize = require('./../db');

const User = sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING,
    secret: Sequelize.STRING
});

const Student = sequelize.define('student',{
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: Sequelize.STRING,
    email: Sequelize.STRING,
    phone: Sequelize.INTEGER
});

module.exports = {
    User,
    Student,
}