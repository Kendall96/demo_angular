const userController = require('./controllers/user');
const studentController = require('./controllers/student');
const QRCode = require('qrcode');
const speakeasy = require("speakeasy");
const qr = require('qr-image');
const fs = require('fs');
 
module.exports.set = (app) => {

    app.get('/user', userController.getUsers);
    app.post('/user', userController.addUser);
    app.put('/user', userController.updateUser);
    app.delete('/user/:id', userController.deleteUser);

    app.get('/qr', function(req, res){
        
        var secret = speakeasy.generateSecret({length: 20});
        QRCode.toDataURL(secret.otpauth_url, function(err, data_url) {
            console.log(data_url);

            res.send({data: data_url, secret: secret.base32}); 

          });

    });

    app.get('/verify/:secret/:token', function(req, res){
        var verified = speakeasy.totp.verify({ secret: req.params.secret,
            encoding: 'base32',
            token: req.params.token });
        res.send({status: 200, valid: verified});
    });

    app.get('/student', studentController.getStudents);
    app.post('/student', studentController.addStudent);
    app.put('/student', studentController.updateStudent);
    app.delete('/student/:id', studentController.deleteStudent);

}
