const studentService = require('./../services/student');

function getStudents(req, res) {
    studentService.getAll()
        .then(data => res.send(data));
};

function getStudent(req, res) {
    studentService.getById(req.params.id)
        .then(data => res.send(data));
}

function addStudent(req, res) {
    
    studentService.addStudent({
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone,
        })
        .then(data => res.send(data));
};

function updateStudent(req, res) {
    studentService.updateStudent({
            id: req.body.id,
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone,
        })
        .then(data2 => res.send(data2));
};

function deleteStudent(req, res) {
    res.send(studentService.deleteStudent({
        id: req.params.id
    }));
};


module.exports = {
    getStudents,
    getStudent,
    addStudent,
    updateStudent,
    deleteStudent
}