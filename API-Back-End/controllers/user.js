const userService = require('./../services/user');
const speakeasy = require("speakeasy");

function getUsers(req, res) {
    userService.getAll()
        .then(data => res.send(data));
};

function getUser(req, res) {
    userService.getById(req.params.id)
        .then(data => res.send(data));
}

function addUser(req, res) {
    userService.addUser({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
            secret: req.body.secret
        })
        .then(data => res.send(data));
};

function updateUser(req, res) {
    userService.updateUser({
            id: req.body.id,
            username: req.body.username,
            email: req.body.email,
            password: req.body.password
        })
        .then(data2 => res.send(data2));
};

function deleteUser(req, res) {
    res.send(userService.deleteUser({
        id: req.params.id
    }));
};


module.exports = {
    getUsers,
    getUser,
    addUser,
    updateUser,
    deleteUser
}