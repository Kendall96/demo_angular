students = require('./../models/models').Student;

const getById = id => students.find(id);
const getAll = () => students.findAll();
const addStudent = student => students.create(student);
const updateStudent = student => students.update(student, {
    where: {
        id: student.id
    }
});


const deleteStudent = student => {
    students.findById(student.id).then((result) => {
        console.log(result);
        return students.destroy({
                where: {
                    id: student.id
                }
            })
            .then((u) => {
                return result
            });

    });

}

module.exports = {
    getById,
    getAll,
    addStudent,
    updateStudent,
    deleteStudent
};