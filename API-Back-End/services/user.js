users = require('./../models/models').User;

const getById = id => users.find(id);
const getAll = () => users.findAll();
const addUser = user => users.create(user);
const updateUser = user => users.update(user, {
    where: {
        id: user.id
    }
});


const deleteUser = user => {
    users.findById(user.id).then((result) => {
        console.log(result);
        return user.destroy({
                where: {
                    id: user.id
                }
            })
            .then((u) => {
                return result
            });

    });

}

module.exports = {
    getById,
    getAll,
    addUser,
    updateUser,
    deleteUser
};