export class User{
    id: number;
    username: string;
    email: string;
    password: string;
    secret: string;
}