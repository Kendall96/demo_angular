import { Injectable } from '@angular/core'; 
import {
  HttpClient,
  HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { User } from './../models/user';
import { QR } from './../models/qr';
import { Token } from './../models/token';
import { Student } from './../models/student';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }
  
  getQR(): Observable<QR>{
    return this.http.get<QR>('http://localhost:8000/qr');
  }

  addUser(data: User): Observable<User>{
    return this.http.post<User>('http://localhost:8000/user',data);
  }

  getUsers(): Observable<User[]>{
    return this.http.get<User[]>('http://localhost:8000/user');
  }

  verifyToken(secret, token): Observable<Token>{
    return this.http.get<Token>('http://localhost:8000/verify/' + secret + '/' + token);
  }

  addStudent(student: Student): Observable<Student>{
    return this.http.post<Student>('http://localhost:8000/student',student);
  }

  getStudents(): Observable<Student[]>{
    return this.http.get<Student[]>('http://localhost:8000/student');
  }

  deleteStudent(id: number){
    return this.http.delete('http://localhost:8000/student/' + id);
  }

  updateStudent(student: Student): Observable<Student>{
    return this.http.put<Student>('http://localhost:8000/student',student);
  }

}
