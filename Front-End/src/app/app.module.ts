import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AdminComponent } from './admin/admin.component';
import { DataService } from './services/data.service';
import { HttpClientModule, HttpClient } from "@angular/common/http";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { Ng2IziToastModule, Ng2IzitoastService } from 'ng2-izitoast';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    Ng2IziToastModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [DataService, Ng2IzitoastService],
  bootstrap: [AppComponent]
})
export class AppModule { }
