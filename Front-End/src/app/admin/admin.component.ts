import { Component, OnInit, Input } from '@angular/core';
import { Student } from './../models/student';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { DataService } from './../services/data.service';
import { NgForm } from '@angular/forms';
import { Ng2IzitoastService } from 'ng2-izitoast';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  constructor(private modalService: NgbModal, private router: Router, private dataService: DataService, public iziToast: Ng2IzitoastService) { }
  @Input() student: Student;
  closeResult: string;
  Student: Student;
  name: string;
  email: string;
  phone: number;
  students: Student[] = [];

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  onGetInfo(orderToEdit: Student) {
    this.Student = new Student();
    this.Student = Object.assign({}, orderToEdit);
  }

  onEdit(id: number) {
    let filterStudent = this.students.filter((order: Student) => order.id === id);
    this.onGetInfo(filterStudent[0]);
  }


  ngOnInit() {
    this.getStudents();
  }

  addStudent(form: NgForm){
    let student = this.students.find(x => x.email == this.email);
    if(student == undefined){
      if (this.student === undefined) {
        this.student = new Student();
      }
      this.student.email = this.email;
      this.student.name = this.name;
      this.student.phone = this.phone;

      this.dataService.addStudent(this.student).subscribe(data => {
        form.reset();
        this.modalService.dismissAll();
        this.iziToast.success({title: "Successfully",
        message: 'The student was created!', position: 'topRight'});
        this.getStudents();
      });
    }else{
      this.iziToast.error({title: "Error",
      message: 'The student already exists!', position: 'topRight'});
    }
  }

  deleteStudent(id: number){
    this.dataService.deleteStudent(id).subscribe(data => {
      this.getStudents();
      this.iziToast.success({title: "Successfully",
      message: 'The student was deleted!', position: 'topRight'});
    });
  }

  closeSession(){
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }

  getStudents(){
    this.dataService.getStudents().subscribe(data => {
      this.students = data;
      console.log(this.students);
    });
  }

  updateStudent(){
    if (this.student === undefined) {
      this.student = new Student();
    }
    this.student.id = this.Student.id;
    this.student.email = this.Student.email;
    this.student.name = this.Student.name;
    this.student.phone = this.Student.phone;

    this.dataService.updateStudent(this.student).subscribe(data => {
      this.modalService.dismissAll();
      this.iziToast.success({title: "Successfully",
      message: 'The student was updated!', position: 'topRight'});
      this.getStudents();
    });
  }

}
