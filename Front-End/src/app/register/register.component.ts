import { Component, OnInit, Input } from '@angular/core';
import { DataService } from './../services/data.service';
import { User } from './../models/user';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import * as sha1 from 'js-sha512';

import { Ng2IzitoastService } from 'ng2-izitoast';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  @Input() user: User;
  username: string;
  email: string;
  password: string;
  confirmpassword: string;
  secret: string;
  img: any; 
  
  users: User[] = [];

  constructor(private dataService: DataService, private router: Router, public iziToast: Ng2IzitoastService) { }

  getIMG(){
    this.dataService.getQR().subscribe(data => {
      console.log(data);
      this.img = data.data;
      this.secret = data.secret;
    });
  }

  getUsers(){
    this.dataService.getUsers().subscribe(data => {
      this.users = data;
      console.log(this.users);
    });
  }

  addUser(form: NgForm){
    
    let user = this.users.find(x=> x.email == this.email);

    if(this.password != this.confirmpassword){
      this.iziToast.error({title: "Error",
      message: 'Passwords did not match!', position: 'topRight'});
    }else if(user == undefined){
      if (this.user === undefined) {
        this.user = new User();
      }
      this.user.email = this.email;
      this.user.username = this.username;
      this.user.password = sha1.sha512(this.password);
      this.user.secret = this.secret;

      this.dataService.addUser(this.user).subscribe(data => {
        form.reset();
        this.router.navigate(['/login']);
      });
    }else{
      this.iziToast.error({title: "Error",
      message: 'The user already exists!', position: 'topRight'});
      form.reset();
    }
  }

  ngOnInit() {
    this.getIMG();
    this.getUsers();
  }

}
